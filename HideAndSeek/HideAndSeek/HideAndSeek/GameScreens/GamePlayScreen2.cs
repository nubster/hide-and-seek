﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XLibrary;
using XLibrary.TileEngine;
using HideAndSeek.Components;
using XLibrary.SpriteClasses;

namespace HideAndSeek.GameScreens
{
    public class GamePlayScreen : BaseGameState
    {
        #region Field Region

        Engine engine = new Engine(32, 32);
        TileMap map;
        Player player;
        AnimatedSprite sprite;
        //---Makha's code---//
        Tileset faces;
        int currentFace;
        HidePlace hp;
        //---End of Makha' code---//

        bool movingToMouse = false;
        Vector2 newLoc = Vector2.Zero;
        Vector2 lastDistance = Vector2.Zero;

        #endregion

        #region Property Region
        #endregion

        #region Constructor Region

        public GamePlayScreen(Game game, GameStateManager manager)
            : base(game, manager)
        {
            player = new Player(game);
            currentFace = 0;//Makha's code
            //faces = new Tileset();
        }

        #endregion

        #region XNA Method Region

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            //---Makha's code---//
            //initiaizing HidePlace object
            Texture2D spriteSheet = Game.Content.Load<Texture2D>(@"HidePlaces\girl1_face3");
            hp = new HidePlace(spriteSheet, 200, 200);
            //---End of Makha's code---//
            spriteSheet = Game.Content.Load<Texture2D>(@"PlayerSprites\boy_tileset1_768x256");
            Dictionary<AnimationKey, Animation> animations = new Dictionary<AnimationKey, Animation>();

            Animation animation = new Animation(3, 256, 256, 0, 0);
            animations.Add(AnimationKey.Down, animation);

            animation = new Animation(3, 256, 256, 0, 0);
            animations.Add(AnimationKey.Left, animation);

            animation = new Animation(3, 256, 256, 0, 0);
            animations.Add(AnimationKey.Right, animation);

            animation = new Animation(3, 256, 256, 0, 0);
            animations.Add(AnimationKey.Up, animation);

            sprite = new AnimatedSprite(spriteSheet, animations);

            base.LoadContent();

            Texture2D tilesetTexture = Game.Content.Load<Texture2D>(@"Tilesets\tileset1");
            Tileset tileset1 = new Tileset(tilesetTexture, 8, 8, 32, 32);

            tilesetTexture = Game.Content.Load<Texture2D>(@"Tilesets\tileset2");
            Tileset tileset2 = new Tileset(tilesetTexture, 8, 8, 32, 32);

            //---Makha's code---//
            tilesetTexture = Game.Content.Load<Texture2D>(@"Tilesets\girl1_face_tileset1");
            faces = new Tileset(tilesetTexture, 3, 1, 256, 256);
            //---end of Makha's code---//

            List<Tileset> tilesets = new List<Tileset>();
            tilesets.Add(tileset1);
            tilesets.Add(tileset2);
            //---Makha's code---//
            tilesets.Add(faces);
            //---end of Makha's code---//

            MapLayer layer = new MapLayer(40, 40);

            for (int y = 0; y < layer.Height; y++)
            {
                for (int x = 0; x < layer.Width; x++)
                {
                    Tile tile = new Tile(0, 0);

                    layer.SetTile(x, y, tile);
                }
            }

            MapLayer splatter = new MapLayer(40, 40);

            Random random = new Random();

            for (int i = 0; i < 80; i++)
            {
                int x = random.Next(0, 40);
                int y = random.Next(0, 40);
                int index = random.Next(2, 14);

                Tile tile = new Tile(index, 0);
                splatter.SetTile(x, y, tile);
            }

            splatter.SetTile(1, 0, new Tile(0, 1));
            splatter.SetTile(2, 0, new Tile(2, 1));
            splatter.SetTile(3, 0, new Tile(0, 1));

            List<MapLayer> mapLayers = new List<MapLayer>();
            mapLayers.Add(layer);
            mapLayers.Add(splatter);

            map = new TileMap(tilesets, mapLayers);
        }

        public override void Update(GameTime gameTime)
        {
            player.Update(gameTime);
            sprite.Update(gameTime);

            Vector2 motion = new Vector2();
            Vector2 lastPosition = sprite.Position;

            //Shashank's code
            Console.Out.Write(GameRef.HandPosition + "\n");
            //End Shashank's code

            if (InputHandler.mouseReleased()) // mouse click detected
            {
                Vector2 clickLoc = InputHandler.lastMouseReleasePoint();
                if (clickLoc.X >= 0 && clickLoc.X < player.Camera.ViewportRectangle.Width &&
                        clickLoc.Y >= 0 && clickLoc.Y <= player.Camera.ViewportRectangle.Height)
                {
                    Vector2 currLoc = new Vector2(sprite.Position.X, sprite.Position.Y);
                    newLoc = new Vector2(MathHelper.Clamp(clickLoc.X + player.Camera.Position.X, 0, TileMap.WidthInPixels),
                                        MathHelper.Clamp(clickLoc.Y + player.Camera.Position.Y, 0, TileMap.HeightInPixels));
                    Console.Out.Write(newLoc.X + " " + newLoc.Y + "\n");
                }
                movingToMouse = true; //set to persist across multiple updates
            }
            else if (movingToMouse)
            {
                Vector2 currLoc = new Vector2(sprite.Position.X,
                                              sprite.Position.Y);
                if (newLoc.X > currLoc.X)
                {
                    sprite.CurrentAnimation = AnimationKey.Right;
                    motion.X = 1;
                }
                else if (newLoc.X < currLoc.X)
                {
                    sprite.CurrentAnimation = AnimationKey.Left;
                    motion.X = -1;
                }

                if (newLoc.Y > currLoc.Y)
                {
                    sprite.CurrentAnimation = AnimationKey.Down;
                    motion.Y = 1;
                }
                else if (newLoc.Y < currLoc.Y)
                {
                    sprite.CurrentAnimation = AnimationKey.Up;
                    motion.Y = -1;
                }

                //Console.Out.Write(motion.X + " " + motion.Y + "\n");
                Vector2 distance = new Vector2(Math.Abs(newLoc.X - sprite.Position.X), Math.Abs(newLoc.Y - sprite.Position.Y));
                if (distance.X < 2) distance.X = 0;
                if (distance.Y < 2) distance.Y = 0;

                //this is a hack-ish way to stop the character from spazzing out near the edges
                Vector2 distanceDiff = new Vector2(Math.Abs(distance.X - lastDistance.X), Math.Abs(distance.Y - lastDistance.Y));
                lastDistance = distance;
                //Console.Out.Write("distance: " + distanceDiff.X + " " + distanceDiff.Y + "\n");
                if ((distance.X == 0 && distance.Y == 0) || (distanceDiff.X < 1 && distanceDiff.Y < 1))
                //Camera has reached the final position (there should probably be a more elegant way to do this)
                {
                    movingToMouse = false;
                }
            }


            if (InputHandler.KeyDown(Keys.W) ||
                InputHandler.ButtonDown(Buttons.LeftThumbstickUp, PlayerIndex.One))
            {
                sprite.CurrentAnimation = AnimationKey.Up;
                motion.Y = -1;
            }
            else if (InputHandler.KeyDown(Keys.S) ||
                InputHandler.ButtonDown(Buttons.LeftThumbstickDown, PlayerIndex.One))
            {
                sprite.CurrentAnimation = AnimationKey.Down;
                motion.Y = 1;
            }

            if (InputHandler.KeyDown(Keys.A) ||
                InputHandler.ButtonDown(Buttons.LeftThumbstickLeft, PlayerIndex.One))
            {
                sprite.CurrentAnimation = AnimationKey.Left;
                motion.X = -1;
            }
            else if (InputHandler.KeyDown(Keys.D) ||
                InputHandler.ButtonDown(Buttons.LeftThumbstickRight, PlayerIndex.One))
            {
                sprite.CurrentAnimation = AnimationKey.Right;
                motion.X = 1;
            }

            if (motion != Vector2.Zero)
            {
                sprite.IsAnimating = true;
                motion.Normalize();

                sprite.Position += motion * sprite.Speed;
                sprite.LockToMap();

                if (player.Camera.CameraMode == CameraMode.Follow)
                    player.Camera.LockToSprite(sprite);
            }
            else
            {
                sprite.IsAnimating = false;
            }

            if (InputHandler.KeyReleased(Keys.F) ||
                InputHandler.ButtonReleased(Buttons.RightStick, PlayerIndex.One))
            {
                player.Camera.ToggleCameraMode();
                if (player.Camera.CameraMode == CameraMode.Follow)
                    player.Camera.LockToSprite(sprite);
            }

            if (player.Camera.CameraMode != CameraMode.Follow)
            {
                if (InputHandler.KeyReleased(Keys.C) ||
                    InputHandler.ButtonReleased(Buttons.LeftStick, PlayerIndex.One))
                {
                    player.Camera.LockToSprite(sprite);
                }
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GameRef.SpriteBatch.Begin(
                SpriteSortMode.Immediate,
                BlendState.AlphaBlend,
                SamplerState.PointClamp,
                null,
                null,
                null,
                Matrix.Identity);

            map.Draw(GameRef.SpriteBatch, player.Camera);
            hp.Draw(gameTime, GameRef.SpriteBatch, player.Camera);//hidePlace is being drawn
            sprite.Draw(gameTime, GameRef.SpriteBatch, player.Camera);
            //---Makha's code---//
            Rectangle destination = new Rectangle(0, 0, 128, 128);
            GameRef.SpriteBatch.Draw(
            faces.Texture,
            destination,
            faces.SourceRectangles[currentFace],
            Color.White);
            //---End of Makha's code---//

            //---Shashank's code---//
            Vector2 handXY = new Vector2(GameRef.HandPosition.X, GameRef.HandPosition.Y);
            GameRef.SpriteBatch.Draw(GameRef.Hand, handXY, Color.White);
            //---End of Shashank's code---//

            base.Draw(gameTime);

            GameRef.SpriteBatch.End();
        }

        #endregion

        #region Abstract Method Region
        #endregion
    }
}
