﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;

using XLibrary;
using XLibrary.Controls;

namespace HideAndSeek.GameScreens
{
   public  class FoundIt: GameState
    {

        #region field region
        LinkLabel restartGame;
        LinkLabel backToMenu;

        PictureBox arrowImage;

        float maxItemWidth = 0f;
        #endregion

        #region constructor region
        public FoundIt(Game game, GameStateManager manager)
            : base(game, manager)
        {
        }

        #endregion

        #region XNA Method Region

        public override void Initialize()
        {
            base.Initialize();
        }


        protected override void LoadContent()
        {
            base.LoadContent();

            ContentManager Content = Game.Content;

           PictureBox backgroundImage = new PictureBox(
                Content.Load<Texture2D>(@"Backgrounds\winning"),
                GameRef.ScreenRectangle);
            ControlManager.Add(backgroundImage);


            Texture2D arrowTexture = Content.Load<Texture2D>(@"GUI\leftarrowUp");

            arrowImage = new PictureBox(
                arrowTexture,
                new Rectangle(
                    0,
                    0,
                    arrowTexture.Width,
                    arrowTexture.Height));
            ControlManager.Add(arrowImage);

            //Mike's
            SoundEffect fanfare = Game.Content.Load<SoundEffect>(@"Music\fanfare");

            fanfare.Play();

            //****end of Mike's****

            restartGame = new LinkLabel();
            restartGame.Text = "Play Again";
            restartGame.Size = restartGame.SpriteFont.MeasureString(restartGame.Text);
            restartGame.Selected += new EventHandler(menuItem_Selected);
            restartGame.SelectedColor = Color.Aqua;

            ControlManager.Add(restartGame);

           backToMenu = new LinkLabel();
           backToMenu.Text = "Back to Main Menu";
           backToMenu.Size = backToMenu.SpriteFont.MeasureString(backToMenu.Text);
           backToMenu.Selected += new EventHandler(menuItem_Selected);
           backToMenu.SelectedColor = Color.Aqua;

            ControlManager.Add(backToMenu);

            ControlManager.NextControl();

            ControlManager.FocusChanged += new EventHandler(ControlManager_FocusChanged);

            Vector2 position = new Vector2(350, 300);
            foreach (Control c in ControlManager)
            {
                if (c is LinkLabel)
                {
                    if (c.Size.X > maxItemWidth)
                        maxItemWidth = c.Size.X;

                    c.Position = position;
                    position.Y += c.Size.Y + 5f;
                }
            }

            ControlManager_FocusChanged(restartGame, null);
        }

        void ControlManager_FocusChanged(object sender, EventArgs e)
        {
            Control control = sender as Control;
            Vector2 position = new Vector2(control.Position.X + maxItemWidth + 10f, control.Position.Y);
            arrowImage.SetPosition(position);
        }


        private void menuItem_Selected(object sender, EventArgs e)
        {
            if (sender == restartGame)
            {
                GamePlayScreen lulz = new GamePlayScreen(Game,StateManager);
                StateManager.PushState(lulz);
            }

            if (sender == backToMenu)
            {
                StateManager.PushState(GameRef.StartMenuScreen);
            }
        }


        #endregion

        #region update region
        public override void Update(GameTime gameTime)
        {
            ControlManager.Update(gameTime, PlayerIndex.One);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GameRef.SpriteBatch.Begin();

            ControlManager.Draw(GameRef.SpriteBatch);

            base.Draw(gameTime);

            GameRef.SpriteBatch.End();
        }


        #endregion
    }
}
