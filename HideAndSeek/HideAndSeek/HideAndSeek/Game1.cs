using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Kinect;

using XLibrary;
using HideAndSeek.GameScreens;

namespace HideAndSeek
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        #region XNA Field Region

        GraphicsDeviceManager graphics;
        public SpriteBatch SpriteBatch;
        SoundEffect soundEffect;
        InputHandler ih;
        Texture2D hand;

        public InputHandler IH
        {
            get { return ih; }
        }

        public Texture2D Hand
        {
            get { return hand; }
        }

        #endregion

        #region Game State Region

        GameStateManager stateManager;

        public TitleScreen TitleScreen;
        public StartMenuScreen StartMenuScreen;
     //   public GamePlayScreen GamePlayScreen;
        public StatsScreen StatsScreen;
        public AchievementsScreen AchievementsScreen;
        public OptionsScreen OptionsScreen;
        public FoundIt FoundIt;

        #endregion

        #region Screen Field Region

        const int screenWidth = 1024;
        const int screenHeight = 768;

        public readonly Rectangle ScreenRectangle;

        #endregion

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);

            IsMouseVisible = true;
            graphics.PreferredBackBufferWidth = screenWidth;
            graphics.PreferredBackBufferHeight = screenHeight;

            ScreenRectangle = new Rectangle(
                0,
                0,
                screenWidth,
                screenHeight);

            Content.RootDirectory = "Content";

            ih = new InputHandler(this);
            Components.Add(ih);

            stateManager = new GameStateManager(this);
            Components.Add(stateManager);

            TitleScreen = new TitleScreen(this, stateManager);
            StartMenuScreen = new StartMenuScreen(this, stateManager);
         //   GamePlayScreen = new GamePlayScreen(this, stateManager);
            OptionsScreen = new OptionsScreen(this, stateManager);
            StatsScreen = new StatsScreen(this, stateManager);
            AchievementsScreen = new AchievementsScreen(this, stateManager);
            FoundIt = new FoundIt(this, stateManager);

            stateManager.ChangeState(TitleScreen);
        }

        protected override void Initialize()
        {

            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            //Shashank's code
            hand = Content.Load<Texture2D>("cursor2_128x128");
            soundEffect = Content.Load<SoundEffect>("click");
        }

        protected override void UnloadContent()
        {
            
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            base.Update(gameTime);
        }


        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            base.Draw(gameTime);
        }
    }
}
