﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


using XLibrary.TileEngine;

namespace XLibrary.SpriteClasses
{
    class SwitchText : AnimatedSprite
    {

        #region field region
        Texture2D endImage;
        int oldAnimation;
        Boolean done = false;
        private  const int whenSwitch = 50;
        private int iterations = 0;

        Boolean Finished
        {
            get { return done; }
        }
        #endregion

        #region constructor region

        public SwitchText(Texture2D startImage,Texture2D endImage,  Dictionary<AnimationKey, Animation> aniDict)
            :base(startImage, aniDict)
        {
            this.endImage = endImage;
            IsAnimating = true;

        }
        #endregion

        #region XNA method region
        public void Update(GameTime gameTime)
        {
            
                oldAnimation = this.animations[CurrentAnimation].CurrentFrame;
                base.Update(gameTime);
                //Console.WriteLine("iterations: " + iterations); - Tianyi took it out feel free to add it back if you need it
                if (this.animations[CurrentAnimation].CurrentFrame == oldAnimation)
                {
                    iterations++;
                    if (iterations >= whenSwitch)
                        texture = endImage;

                }
            

        }

        public void Draw(SpriteBatch spriteBatch,Camera camera){
            if (iterations <= whenSwitch * 2)
                base.Draw(spriteBatch, camera);
        }



        #endregion
    }
}
