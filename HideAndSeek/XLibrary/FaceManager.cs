﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XLibrary;
using XLibrary.TileEngine;
using XLibrary.SpriteClasses;

namespace HideAndSeek.GameScreens
{
    public class FaceManager
    {
        #region Field Region
        Texture2D[,] faces;
        int currentFaceSet; // corresponds to the row index of faces array
        int currentFace; // corresponds to the column index of faces array
        int faceNum;//number of columns
        int faceSetNum;//number of rows
        Vector2 position;
        bool isFloating;
        int verticalRange;
        int horizontalRange;
        int iterationNum;
        Vector2 velocity;
        float alpha; // the transparency of the current texture
        bool disappear; // indicates if the face should disappear from the screen

        double scale;
        #endregion

        #region Property Region
        public int CurrentFaceSet
        {
            get { return currentFaceSet; }
        }
        public Vector2 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }
        public bool IsFloating
        {
            get { return isFloating; }
            set { isFloating = value; }
        }
        public int IterationNum
        {
            get { return iterationNum; }
            set { iterationNum = value; }
        }
        #endregion

        #region Constructor Region

        public FaceManager(Texture2D[,] f, int fNum, int fsNum)
        {
            faces = f;
            faceNum = fNum;
            faceSetNum = fsNum;
            currentFaceSet = 0;
            currentFaceSet = 0;
            position.X = 128;
            position.Y = 0;
            isFloating = false;
            iterationNum = 0;
            alpha = 1;
            disappear = true;
        }
        public FaceManager(int fNum, int fsNum)
        {
            faces = new Texture2D[fsNum, fNum];
            faceNum = fNum;
            faceSetNum = fsNum;
            currentFaceSet = 0;
            currentFaceSet = 0;
        }

        #endregion

        #region XNA Method Region



        public void Update(GameTime gameTime)
        {
            if (isFloating)
            {
                // the face starts floating
                float alphaStep = 0.05f;
                disappear = false;
                //position.Y = position.Y - velocity.Y;
                scale = scale * 1.01;
                iterationNum++;
                //if (iterationNum > 30 &&iterationNum !=100 && iterationNum % 5 == 0) alpha = alpha - alphaStep;
                if (iterationNum == 100)
                {
                    //in this part the image floated enought, now time to stop floating and disappear
                    scale = 1;
                    iterationNum = 0;
                    isFloating = false;
                    disappear = true;
                    alpha = 1;
                }
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Camera camera)
        {
            if (!disappear)
            {
                int posX = Convert.ToInt32(position.X);// -Convert.ToInt32(camera.Position.X);
                int posY = Convert.ToInt32(position.Y);// -Convert.ToInt32(camera.Position.Y);
                int width = Convert.ToInt32(95 * scale);
                int height = Convert.ToInt32(128 * scale);
                Rectangle destination = new Rectangle(posX, posY, width, height);
                Rectangle sourceRec = new Rectangle(0, 0, faces[currentFaceSet, currentFace].Width, faces[currentFaceSet, currentFace].Height);
                spriteBatch.Draw(
                faces[currentFaceSet, currentFace],
                destination,
                sourceRec,
                new Color(new Vector4(1f, 1f, 1f, alpha)));
            }
        }
        public void addFace(Texture2D face, int faceSetPos, int facePos)
        {
            faces[faceSetPos, facePos] = face;
        }
        public void setCurrentFace(int faceSetIndex, int faceIndex)
        {
            currentFaceSet = faceSetIndex;
            currentFace = faceIndex;
        }
        public void setFloatParamiters(Vector2 startPosition, int verticalRange, int horizontalRange)
        {
            position = startPosition;
            this.verticalRange = verticalRange;
            this.horizontalRange = horizontalRange;
        }
        #endregion
    }
}
