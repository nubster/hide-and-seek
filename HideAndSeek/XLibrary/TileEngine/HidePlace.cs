﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace XLibrary.TileEngine
{
    public class HidePlace
    {
        #region Field Region

        Vector2 position;
        Texture2D texture;
        float imageHeight;
        float imageWidth;
        Rectangle drawRec;
        Rectangle activeRec;
        Rectangle baseRec;
        enum HidePlaceType { tree = 1, bush, slide, horse, elephant, sandbox, trash };

        #endregion

        #region Property Region
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }
        public Rectangle ActiveRec
        {
            get { return activeRec; }
        }
        public Rectangle DrawRec
        {
            get { return drawRec; }
        }
        public Rectangle BaseRec
        {
            get { return baseRec; }
        }
        #endregion

        #region Constructor Region
        public HidePlace(Texture2D sprite, Vector2 pos, float height, float width, int hp)
        {
            texture = sprite;
            imageHeight = height;
            imageWidth = width;
            if (hp == (int)HidePlaceType.tree)
            {
                drawRec = new Rectangle(0, 0, (int)imageHeight, (int)imageWidth);
                //activeRec = new Rectangle(68, 20, 120, 140);
                activeRec = new Rectangle(68, 15, 120, 220);
                baseRec = new Rectangle(68, 160, 120, 70);
            }
            else if (hp == (int)HidePlaceType.bush)
            {
                drawRec = new Rectangle(0, 0, (int)imageHeight, (int)imageWidth);
                //activeRec = new Rectangle(20, 10, 205, 130);
                activeRec = new Rectangle(20, 8, 215, 230);
                baseRec = new Rectangle(20, 140, 205, 95);
            }
            else if (hp == (int)HidePlaceType.slide)
            {
                drawRec = new Rectangle(0, 0, (int)imageHeight, (int)imageWidth);
                activeRec = new Rectangle(22, 30, 292, 320);
                baseRec = new Rectangle(20, 140, 205, 95);

            }
            else if (hp == (int)HidePlaceType.horse)
            {
                drawRec = new Rectangle(0, 0, (int)imageHeight, (int)imageWidth);
                //activeRec = new Rectangle(80, 5, 160, 195);
                activeRec = new Rectangle(75, 5, 165, 245);
                baseRec = new Rectangle(80, 200, 160, 50);
            }
            else if (hp == (int)HidePlaceType.elephant)
            {
                drawRec = new Rectangle(0, 0, (int)imageHeight, (int)imageWidth);
                activeRec = new Rectangle(10, 0, 235, 252);
                baseRec = new Rectangle(80, 200, 160, 50);
            }
            else if (hp == (int)HidePlaceType.sandbox)
            {
                drawRec = new Rectangle(65, 0, 130, 250);
                activeRec = new Rectangle(0, 0, (int)imageHeight, (int)imageWidth);
            }
            else if (hp == (int)HidePlaceType.trash)
            {
                drawRec = new Rectangle(0, 0, (int)imageHeight, (int)imageWidth);
                activeRec = new Rectangle(0, 0, (int)imageHeight, (int)imageWidth);
            }
            position = new Vector2(pos.X, pos.Y);
        }
        #endregion

        #region Method Region
        public void Update(GameTime gameTime)
        {
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Camera camera, bool isBehind)
        {
            Texture2D temp = texture;
            float alpha = 100;
            if (isBehind)
            {
                alpha = 0.5f;
            }
            spriteBatch.Draw(
                temp,
                position - camera.Position,
                activeRec,
                new Color(new Vector4(1f, 1f, 1f, alpha)));

        }
        public void CollisionCheck()
        {

        }
        #endregion
    }
}
