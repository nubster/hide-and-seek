﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XLibrary.SpriteClasses;

namespace XLibrary.TileEngine
{
    public enum CameraMode { Free, Follow }

    public class Camera
    {
        #region Field Region

        Vector2 position;
        Vector2 lastPosition;
        Vector2 newLoc;
        float speed;
        float zoom;
        Rectangle viewportRectangle;
        CameraMode mode;
        bool movingToMouse;

        #endregion

        #region Property Region

        public Rectangle ViewportRectangle
        {
            get { return viewportRectangle; }
        }

        public Vector2 Position
        {
            get { return position; }
            private set { position = value; }
        }

        public float Speed
        {
            get { return speed; }
            set
            {
                speed = (float)MathHelper.Clamp(speed, 1f, 16f);
            }
        }

        public float Zoom
        {
            get { return zoom; }
        }

        public CameraMode CameraMode
        {
            get { return mode; }
        }

        #endregion

        #region Constructor Region

        public Camera(Rectangle viewportRect)
        {
            speed = 4f;
            zoom = 1f;
            viewportRectangle = viewportRect;
            mode = CameraMode.Follow;
        }

        public Camera(Rectangle viewportRect, Vector2 position)
        {
            speed = 4f;
            zoom = 1f;
            viewportRectangle = viewportRect;
            Position = position;
            mode = CameraMode.Follow;
        }

        #endregion

        #region Method Region

        public void Update(GameTime gameTime)
        {
            if (mode == CameraMode.Follow)
                return;
            /*
            Vector2 motion = Vector2.Zero;
            if (movingToMouse)
            {
                Vector2 clickLoc = InputHandler.lastMouseReleasePoint();
                Vector2 currLoc = new Vector2(position.X + viewportRectangle.Width / 2,
                                                position.Y + viewportRectangle.Height / 2);
                motion.X = newLoc.X - currLoc.X;
                motion.Y = newLoc.Y - currLoc.Y;
                //Console.Out.Write(motion.X + " " + motion.Y + "\n");
                if (Math.Abs(motion.X) < 2) motion.X = 0;
                if (Math.Abs(motion.Y) < 2) motion.Y = 0;

                if ((Math.Abs(motion.X) == 0 && Math.Abs(motion.Y) == 0) || lastPosition == position)
                //Camera has reached the final position (there should probably be a more elegant way to do this)
                {
                    movingToMouse = false;
                }
            }
            else if (InputHandler.mouseReleased()) // mouse click detected
            {
                Vector2 clickLoc = InputHandler.lastMouseReleasePoint();
                if (clickLoc.X >= 0 && clickLoc.X <= viewportRectangle.Width &&
                    clickLoc.Y >= 0 && clickLoc.Y <= viewportRectangle.Height)
                {
                    Vector2 currLoc = new Vector2(position.X + viewportRectangle.Width / 2,
                                                  position.Y + viewportRectangle.Height / 2);
                    newLoc = new Vector2(MathHelper.Clamp(position.X + clickLoc.X, 0, TileMap.WidthInPixels),
                                        MathHelper.Clamp(position.Y + clickLoc.Y, 0, TileMap.HeightInPixels));
                    motion.X = newLoc.X - currLoc.X;
                    motion.Y = newLoc.Y - currLoc.Y;
                }
                movingToMouse = true; //set to persist across multiple updates
            }
            else // no mouse input detected; checking for keyboard input
            {
                if (InputHandler.KeyDown(Keys.Left))
                    motion.X = -speed;
                else if (InputHandler.KeyDown(Keys.Right))
                    motion.X = speed;

                if (InputHandler.KeyDown(Keys.Up))
                    motion.Y = -speed;
                else if (InputHandler.KeyDown(Keys.Down))
                    motion.Y = speed;
            }
            if (motion != Vector2.Zero)
                motion.Normalize();
            lastPosition = position;
            position += motion * speed;

            LockCamera();*/
        }
            
        private void LockCamera()
        {
            position.X = MathHelper.Clamp(position.X,
                0,
                TileMap.WidthInPixels - viewportRectangle.Width);
            position.Y = MathHelper.Clamp(position.Y,
                0,
                TileMap.HeightInPixels - viewportRectangle.Height);
        }

        public void LockToSprite(AnimatedSprite sprite)
        {
            position.X = sprite.Position.X + sprite.Width / 2
                            - (viewportRectangle.Width / 2);
            position.Y = sprite.Position.Y + sprite.Height / 2
                            - (viewportRectangle.Height / 2);
            LockCamera();
        }

        public void ToggleCameraMode()
        {
            if (mode == CameraMode.Follow)
                mode = CameraMode.Free;
            else if (mode == CameraMode.Free)
                mode = CameraMode.Follow;
        }

        #endregion
    }
}
