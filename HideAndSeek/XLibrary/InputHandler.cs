using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Kinect;
using HideAndSeek;

namespace XLibrary
{
    public class InputHandler : Microsoft.Xna.Framework.GameComponent
    {
        #region Field Region
        //static Game1 GameRef;
        

        #endregion

        #region Keyboard Field Region

        static KeyboardState keyboardState;
        static KeyboardState lastKeyboardState;

        #endregion

        #region Game Pad Field Region

        static GamePadState[] gamePadStates;
        static GamePadState[] lastGamePadStates;

        #endregion

        #region Mouse Field Region

        static MouseState mouseState;
        static MouseState lastMouseState;
        static Vector2 mouseReleasePoint;

        #endregion

        #region Kinect Field Region

        //static Vector2 HandPosition;

        //Shashank's code
        public static Vector3 hpos
        {
            get { return handPosition; }
        }

        //Mike's*******************
        static float newLeftPos;
        static float oldLeftPos;
        static float totalLeftMovement = 0;
        static int iterations = 0;
        const int checkGestureTime = 50;
        const float checkGestureDistance = 100;
        //*********************************

        #endregion

        #region Keyboard Property Region

        public static KeyboardState KeyboardState
        {
            get { return keyboardState; }
        }

        public static KeyboardState LastKeyboardState
        {
            get { return lastKeyboardState; }
        }

        #endregion

        #region Game Pad Property Region

        public static GamePadState[] GamePadStates
        {
            get { return gamePadStates; }
        }

        public static GamePadState[] LastGamePadStates
        {
            get { return lastGamePadStates; }
        }

        #endregion

        #region Mouse Property Region

        public static MouseState MouseState
        {
            get { return mouseState; }
        }

        public static MouseState LastMouseState
        {
            get { return lastMouseState; }
        }

        public static Vector2 MouseReleasePoint
        {
            get { return mouseReleasePoint; }
        }
        #endregion

        #region Kinect_init

        const int screenWidth = 1024;
        const int screenHeight = 768;

       

        static Vector3 handPosition = new Vector3();
        Vector3 shoulderPosition = new Vector3();
        KinectSensor kinectSensor;
        static bool reachingGestureDetected;
        static bool backingGestureDetected = false;

        /*public Vector3 HandPosition
        {
            get { return handPosition; }
        }*/

        static public bool ReachingGestureDetected
        {
            get
            {
                if (reachingGestureDetected)
                {
                    //soundEffect.Play();
                    reachingGestureDetected = false;
                    return true;
                }
                else return false;
            }
        }

        static public bool BackingGestureDetected
        {
            get
            {
                if (backingGestureDetected)
                {
                    backingGestureDetected = false;
                    return true;
                }
                return false;
            }
        }

        #endregion

        #region Kinect functions

        void KinectSensors_StatusChanged(object sender, StatusChangedEventArgs e)
        {
            if (this.kinectSensor == e.Sensor)
            {
                if (e.Status == KinectStatus.Disconnected ||
                    e.Status == KinectStatus.NotPowered)
                {
                    this.kinectSensor = null;
                    this.DiscoverKinectSensor();
                }
            }
        }

        private bool InitializeKinect()
        {
            // Skeleton Stream
            kinectSensor.SkeletonStream.Enable(new TransformSmoothParameters()
            {
                Smoothing = 0.5f,
                Correction = 0.5f,
                Prediction = 0.5f,
                JitterRadius = 0.05f,
                MaxDeviationRadius = 0.04f
            });
            kinectSensor.SkeletonFrameReady += new EventHandler<SkeletonFrameReadyEventArgs>(kinectSensor_SkeletonFrameReady);

            try
            {
                kinectSensor.Start();
            }
            catch
            {
                return false;
            }
            return true;
        }

        void kinectSensor_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame != null)
                {
                    Skeleton[] skeletonData = new Skeleton[skeletonFrame.SkeletonArrayLength];

                    skeletonFrame.CopySkeletonDataTo(skeletonData);
                    Skeleton playerSkeleton = (from s in skeletonData where s.TrackingState == SkeletonTrackingState.Tracked select s).FirstOrDefault();
                    if (playerSkeleton != null)
                    {
                        Joint rightHand = playerSkeleton.Joints[JointType.HandRight];
                        Joint rightShoulder = playerSkeleton.Joints[JointType.ShoulderRight];
                        Joint leftHand = playerSkeleton.Joints[JointType.HandLeft];

                        handPosition = new Vector3((((1.25f * rightHand.Position.X) + 0.5f) * (screenWidth)), (((-1.25f * rightHand.Position.Y) + 0.5f) * (screenHeight)), (((-0.5f * rightHand.Position.Z) + 0.5f) * (screenWidth)));
                        shoulderPosition = new Vector3((((1.25f * rightShoulder.Position.X) + 0.5f) * (screenWidth)), (((-1.25f * rightShoulder.Position.Y) + 0.5f) * (screenHeight)), (((-0.5f * rightShoulder.Position.Z) + 0.5f) * (screenWidth)));

                        oldLeftPos = newLeftPos;
                        newLeftPos = ((1.25f * leftHand.Position.X) + 0.5f) * (screenWidth);
                      
                        ReachingGesture(handPosition, shoulderPosition);
                    }
                }
            }
        }

        bool ReachingGesture(Vector3 handPosition, Vector3 shoulderPosition)
        {
            if (Math.Abs(handPosition.Z - shoulderPosition.Z) > 200)
            {
                reachingGestureDetected = true;
                return true;
            }
            else
            {
                if (reachingGestureDetected)
                    backingGestureDetected = true;
                else
                    backingGestureDetected = false;
                reachingGestureDetected = false;
                return false;
            }
        }

       public static bool CheckingGesture()
        {
            totalLeftMovement += (oldLeftPos - newLeftPos);
            iterations++;
            //Console.WriteLine("*****totalLeft: "+totalLeftMovement+" ********* iterations: "+iterations);

            bool toRet = false;
            if (iterations > checkGestureTime)
            {                
                if (totalLeftMovement > checkGestureDistance)
                {
                    toRet = true;
                    //Console.WriteLine("*******OMGCHECKED!!!!!!!!!!!!!!!!***************");
                }

                iterations = 0;
                totalLeftMovement = 0;
            }

            return toRet;
            

        }

     

        private void DiscoverKinectSensor()
        {
            foreach (KinectSensor sensor in KinectSensor.KinectSensors)
            {
                if (sensor.Status == KinectStatus.Connected)
                {
                    // Found one, set our sensor to this
                    kinectSensor = sensor;
                    Console.Out.Write("found kinect sensor");
                    break;
                }
                else
                {
                    Console.Out.Write("no kinect sensor found");
                }
            }

            if (this.kinectSensor == null)
            {
                return;
            }

            // Init the found and connected device
            if (kinectSensor.Status == KinectStatus.Connected)
            {
                InitializeKinect();
            }
        }

        #endregion

        #region Constructor Region

        public InputHandler(Game game)
            : base(game)
        {
            keyboardState = Keyboard.GetState();
            mouseState = Mouse.GetState();

            gamePadStates = new GamePadState[Enum.GetValues(typeof(PlayerIndex)).Length];

            foreach (PlayerIndex index in Enum.GetValues(typeof(PlayerIndex)))
                gamePadStates[(int)index] = GamePad.GetState(index);

            //S's code
            KinectSensor.KinectSensors.StatusChanged += new EventHandler<StatusChangedEventArgs>(KinectSensors_StatusChanged);
            DiscoverKinectSensor();
        }

        #endregion

        #region XNA methods

        public override void Initialize()
        {

            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            lastKeyboardState = keyboardState;
            keyboardState = Keyboard.GetState();

            lastMouseState = mouseState;
            mouseState = Mouse.GetState();

            lastGamePadStates = (GamePadState[])gamePadStates.Clone();
            foreach (PlayerIndex index in Enum.GetValues(typeof(PlayerIndex)))
                gamePadStates[(int)index] = GamePad.GetState(index);

            base.Update(gameTime);
        }

        #endregion

        #region General Method Region

        public static void Flush()
        {
            lastKeyboardState = keyboardState;
            lastMouseState = mouseState;
        }

        #endregion

        #region Keyboard Region

        public static bool KeyReleased(Keys key)
        {
            return keyboardState.IsKeyUp(key) &&
                lastKeyboardState.IsKeyDown(key);
        }

        public static bool KeyPressed(Keys key)
        {
            return keyboardState.IsKeyDown(key) &&
                lastKeyboardState.IsKeyUp(key);
        }

        public static bool KeyDown(Keys key)
        {
            return keyboardState.IsKeyDown(key);
        }

        #endregion

        #region Game Pad Region

        public static bool ButtonReleased(Buttons button, PlayerIndex index)
        {
            return gamePadStates[(int)index].IsButtonUp(button) &&
                lastGamePadStates[(int)index].IsButtonDown(button);
        }

        public static bool ButtonPressed(Buttons button, PlayerIndex index)
        {
            return gamePadStates[(int)index].IsButtonDown(button) &&
                lastGamePadStates[(int)index].IsButtonUp(button);
        }

        public static bool ButtonDown(Buttons button, PlayerIndex index)
        {
            return gamePadStates[(int)index].IsButtonDown(button);
        }

        #endregion

        #region Mouse Region

        public static Vector2 mousePosition()
        {
            return new Vector2(mouseState.X, mouseState.Y);
        }

        public static bool mouseReleased()
        {
            if (mouseState.LeftButton == ButtonState.Released &&
                lastMouseState.LeftButton == ButtonState.Pressed)
            {
                mouseReleasePoint = new Vector2(mouseState.X, mouseState.Y);
                return true;
            }
            else return false;

        }

        public static Vector2 lastMouseReleasePoint()
        {
            return mouseReleasePoint;
        }

        #endregion
    }
}
